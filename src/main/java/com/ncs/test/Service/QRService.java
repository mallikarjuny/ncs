package com.ncs.test.Service;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.client.j2se.MatrixToImageConfig;
import com.google.zxing.client.j2se.MatrixToImageWriter;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;
import com.ncs.test.Modal.ShortenUrl;
import org.springframework.stereotype.Service;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.nio.file.FileSystems;
import java.nio.file.Path;
import java.util.HashMap;
import java.util.Map;

@Service
public class QRService {
    public Map<String, ShortenUrl> shortenUrlList = new HashMap<>();

    public String generateQRCodeImage(String text, int width, int height, String filePath)
            throws WriterException, IOException {

        ShortenUrl shortenUrl = new ShortenUrl();
        shortenUrl.setFull_url(text);
        String randomChar = getRandomChars();
        shortenUrl.setShort_url("http://3.23.2.213:8080/s/"+randomChar);
        shortenUrlList.put(randomChar, shortenUrl);

        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        BitMatrix bitMatrix = qrCodeWriter.encode(shortenUrl.getShort_url(), BarcodeFormat.QR_CODE, width, height);

        Path path = FileSystems.getDefault().getPath(filePath);
        MatrixToImageWriter.writeToPath(bitMatrix, "PNG", path);

        return shortenUrl.getShort_url();

    }


    public ShortenUrl getShortUrl(String randomString)  {
        return shortenUrlList.get(randomString);
    }

    public String getRandomChars() {
        String randomStr = "";
        String possibleChars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
        for (int i = 0; i < 5; i++)
            randomStr += possibleChars.charAt((int) Math.floor(Math.random() * possibleChars.length()));
        return randomStr;
    }
}
