package com.ncs.test.Controller;

import com.google.zxing.WriterException;
import com.ncs.test.Modal.ShortenUrl;
import com.ncs.test.Service.QRService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Base64;

@Controller
public class QRController {
    private static final String QR_CODE_IMAGE_PATH = "./src/main/resources/static/img/QRCode.png";

    @Autowired
    QRService qrService;

    @GetMapping("/submit")
    public String getQRCode(Model model,@RequestParam("url") String url) throws MalformedURLException {
        String shortenRUrl="";

        try {
            URL u = new URL(url); // this would check for the protocol
            u.toURI();
            if(!url.isEmpty()) {
                shortenRUrl = qrService.generateQRCodeImage(url, 250, 250, QR_CODE_IMAGE_PATH);
            }

        } catch (WriterException | IOException | URISyntaxException e) {
            e.printStackTrace();
            return "InvalidURL";
        }

        model.addAttribute("url",url);
        model.addAttribute("shortUrl",shortenRUrl);

        return "qrcode";
    }

    @GetMapping(value="/")
    public String get() throws IOException {
       return "index";
    }

    @GetMapping(value="/s/{randomstring}")
    public void getFullUrl(HttpServletResponse response, @PathVariable("randomstring") String randomString) throws IOException {
        System.out.println("here: \n"+randomString );
        response.sendRedirect(qrService.getShortUrl(randomString).getFull_url());
    }
}
